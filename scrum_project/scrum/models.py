from django.db import models
from django.contrib.auth.models import User
import datetime


class UserConfig(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    hours_per_day = models.FloatField(
        "Hours per day",
        default=4.
    )


class Sprint(models.Model):
    start_date = models.DateField(
        "Start date"
    )
    stop_date = models.DateField(
        "Stop date"
    )
    work_on_weekends = models.BooleanField(
        "Work on weekends",
        default=False
    )
    holidays = models.IntegerField(
        "Holidays",
        default=0
    )

    def length(self):
        return \
            (self.stop_date - self.start_date).days + 1\
            - self.holidays

    def __repr__(self):
        return "<Sprint from {} to {}>".format(
            self.start_date,
            self.stop_date
        )

    def sum_estimated_time(queryset):
        return queryset.aggregate(estimated=models.Sum('estimated'))\
            .get('estimated', 0.) or 0.

    def get_time(self, user, query):
        tasks = self.task_set.filter(owner=user)
        if tasks.count() == 0:
            return 0

        user_config, created = UserConfig.objects.get_or_create(user=user)

        if query in Task.STATUS.values():
            return Sprint.sum_estimated_time(tasks.filter(status=query))
        elif query == "unscheduled":
            scheduled = Sprint.sum_estimated_time(tasks.all())
            return self.length() * user_config.hours_per_day\
                - scheduled


class Task(models.Model):
    STATUS = {
        "Todo": 1,
        "Started": 2,
        "Completed": 3,
        "Deferred": 4,
    }
    sprint = models.ForeignKey(
        Sprint,
        on_delete=models.CASCADE
    )
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    name = models.CharField(
        "Name",
        max_length=120
    )
    description = models.TextField(
        "Task description",
        default="",
        blank=True
    )
    status = models.IntegerField(
        "Status",
        choices=[
            (value, key)
            for key, value in STATUS.items()
        ],
        default=STATUS['Todo']
    )
    estimated = models.FloatField(
        "Estimated task duration (hrs)"
    )
    actual = models.FloatField(
        "Actual task duration (hrs)",
        default=0.
    )

