from django.test import TestCase
from scrum.models import Sprint, Task, UserConfig
from django.contrib.auth.models import User
import datetime

class ModelsTests(TestCase):

    def setUp(self):
        # creating users
        self.dev_A = User(username="Alice")
        self.dev_A.save()

        self.dev_B = User(username="Bob")
        self.dev_B.save()
        dev_B_config = UserConfig(
            user=self.dev_B,
            hours_per_day=3.
        )
        dev_B_config.save()

        # creating sprint
        self.sprint = Sprint(
            start_date=datetime.date(2019, 1, 1),
            stop_date=datetime.date(2019, 1, 15),
            holidays=4
        )
        self.sprint.save()

        # creating tasks
        self.task_A1 = Task(
            owner=self.dev_A,
            sprint=self.sprint,
            name="TestCase 01",
            description="Testing this scrum tool",
            estimated=4.,
        )
        self.task_A1.save()

        self.task_A2 = Task(
            owner=self.dev_A,
            sprint=self.sprint,
            name="TestCase 02",
            description="Testing this scrum tool",
            estimated=8.,
        )
        self.task_A2.save()

        self.task_B1 = Task(
            owner=self.dev_B,
            sprint=self.sprint,
            name="TestCase 03",
            description="Testing this scrum tool",
            estimated=4.,
            status=2
        )
        self.task_B1.save()

        self.task_B2 = Task(
            owner=self.dev_B,
            sprint=self.sprint,
            name="TestCase 04",
            description="Testing this scrum tool",
            estimated=8.,
            status=3
        )
        self.task_B2.save()

        self.task_B3 = Task(
            owner=self.dev_B,
            sprint=self.sprint,
            name="TestCase 05",
            description="Testing this scrum tool",
            estimated=8.,
            status=4
        )
        self.task_B3.save()


    def test_sprint_length(self):
        self.assertEqual(self.sprint.length(), 11)

    def test_task_set(self):
        self.assertTrue(self.task_A2 in self.sprint.task_set.all())

    def test_task_owner(self):
        self.assertEqual(self.sprint.task_set.filter(owner=self.dev_A).count(), 2)

    def test_sprint_get_time(self):
        self.assertEqual(self.sprint.get_time(self.dev_A, Task.STATUS['Todo']), 12.)
        self.assertEqual(self.sprint.get_time(self.dev_A, Task.STATUS['Completed']), 0.)
        self.assertEqual(self.sprint.get_time(self.dev_A, 'unscheduled'), 32.)

        self.assertEqual(self.sprint.task_set.filter(owner=self.dev_B).count(), 3)
        self.assertEqual(self.sprint.get_time(self.dev_B, Task.STATUS['Completed']), 8.)
        self.assertEqual(self.sprint.get_time(self.dev_B, 'unscheduled'), 13.)
